<?php

require_once 'Ape.php';
require_once 'Frog.php';

class Animal {
  public $name;
  public $legs = 4;
  public $cold_blooded;

  public function __construct($name) {
    $this->name = $name;
  }

  public function cold_blooded() {
    return $this->cold_blooded = 'no';
  }
}

?>
