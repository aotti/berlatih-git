<?php
require_once 'Animal.php';

$sheep = new Animal("shaun");

echo "<h2> Release 0 </h2>";
echo "<h3> <pre>";
echo "Nama hewan      = " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah kaki     = " . $sheep->legs . "<br>"; // 4
echo "Berdarah dingin = " . $sheep->cold_blooded(); // "no"
echo "</h3>";

$sungokong = new Ape("kera sakti");
// $sungokong->yell() // "Auooo"
echo "<h2> Release 1 </h2>";
echo "<h3> <pre>";
echo "Nama hewan      = " . $sungokong->name . "<br>"; // "kera sakti"
echo "Jumlah kaki     = " . $sungokong->get_legs() . "<br>"; // 2
echo "Berdarah dingin = " . $sungokong->cold_blooded() . "<br>"; // "no"
echo "Yell            = " . $sungokong->yell(); // "no"
echo "<br> <br>";

$kodok = new Frog("buduk");
// $kodok->yell() // "Auooo"
echo "Nama hewan      = " . $kodok->name . "<br>"; // "kera sakti"
echo "Jumlah kaki     = " . $kodok->legs . "<br>"; // 2
echo "Berdarah dingin = " . $kodok->cold_blooded() . "<br>"; // "no"
echo "Yell            = " . $kodok->jump(); // "no"
echo "</h3>";
?>
