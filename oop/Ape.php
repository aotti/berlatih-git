<?php

class Ape extends Animal {
  public function get_legs() {
    return $this->legs = 2;
  }

  public function yell() {
    return "Auooo";
  }
}

?>
